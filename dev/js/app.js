"use strict";

var auth__user_name;
var x_name;
var auth__enter;


function logOut(){
	
	setTimeout(function () {
		
		$(".auth__button").removeClass("auth__exit")
		.addClass("auth__enter");
		$(".auth__user_name").removeClass("active");
		auth__enter = document.getElementsByClassName('auth__enter')[0];
		auth__enter.innerHTML = "Войти";
		x_name = "Вы не авторизованы";
		addDOM();

	}, 100);
	
};


function login() { // Имитация авторизации
	
	var form__login = document.forms.__form.elements[0].value;
	var form__password = document.forms.__form.elements[1].value;

	$(".auth__button").removeClass("auth__enter")
	.addClass("auth__exit");

	$(".auth__user_name").addClass("active");	
	
	var auth__exit = document.getElementsByClassName('auth__exit')[0];

	auth__exit.innerHTML = "Выйти";

	if(form__login && form__password !== ""){ // какие-то проверки

		$(".pop-up").fadeOut(200);
		
		x_name = form__login;

	} else {
		alert("Введите логин и пароль"); // какие-то подсказки
	};

	addDOM();

	
};


function addDOM(){
	
	var button = document.getElementsByClassName('auth__button')[0];

	auth__user_name = document.getElementsByClassName('auth__user_name')[0];
	auth__user_name.innerHTML = x_name;

	$(document).mouseup(function (e) { 
	
		$('.auth__enter').click(function() {
			if(button.classList.contains('auth__enter')) {			
				$(".pop-up").fadeIn(200);
			} 	else {
					logOut();
				}
		});
	
		var div = $(".pop-up__inner");
		
		if (!div.is(e.target) && div.has(e.target).length === 0) { 
				$(".pop-up").fadeOut(200); 
			}
		
	});

	$(document).on( "keydown", function(e) {
		if(e.which == 27) {
			$(".pop-up").fadeOut(200); 
		}
	});


};

logOut();
