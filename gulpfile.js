"use strict";

const gulp   	  = require('gulp'),
	  sass   	  = require('gulp-sass'),
	  rename 	  = require('gulp-rename'),
	  browserSync = require('browser-sync'),
	  uglify 	  = require('gulp-uglifyjs'),
	  del  		  = require('del'),
	  autoprefixer= require('gulp-autoprefixer');

gulp.task('sass', function(){
	return gulp.src('dev/sсss/**/*.sсss')
	.pipe(sass())
	.pipe(autoprefixer(['last 15 versions', '>1%', 'ie 8'], {cascade: true}))
	.pipe(rename({basename:'style'}))
	.pipe(gulp.dest('dev/css'))
	.pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', ['libs'], function(){
	return gulp.src ('dev/js/app.js')
	.pipe(uglify())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('dev/js'));
});

gulp.task('libs', function(){
	return gulp.src ('bower_components/jquery/dist/jquery.min.js')
	.pipe(gulp.dest('dev/js'));
})
gulp.task('browser-sync', function(){
	browserSync.init({
		server: {
			baseDir : 'dev'
		},
		notify: false //отключаем уведомления
	});
});

gulp.task('watch', ['browser-sync', 'scripts'], function(){
	gulp.watch('dev/sсss/**/*.sсss' , ['sass']);
	gulp.watch('dev/**/*.html', browserSync.reload);
	gulp.watch('dev/**/*.js', browserSync.reload);
});


gulp.task('clean', function(){
	return del.sync('build');
})

gulp.task('build', ['clean', 'sass', 'scripts'], function(){

	//css
	var buildCss = gulp.src('dev/css/**/*.css')
		.pipe(gulp.dest('build/css'));
	
	//js
	var buildJs = gulp.src([
		'dev/js/**/*.js',
		'bower_components/jquery/dist/jquery.min.js'])
		.pipe(gulp.dest('build/js'));
	
	//html
	var buildHtml = gulp.src('dev/*.html')
		.pipe(gulp.dest('build'));


});
